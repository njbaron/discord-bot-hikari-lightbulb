# Discord Bot Hikari - Lightbulb

A test of the hikari - Lightbulb discord bot api in preparation for discord.py obsolescence.

## Running the bot

Create an `.env` file with the following contents filled in.

```shell
TOKEN=<token>
GUILD_ID=<guild to update with slash commands>
LOG_CHANNEL_ID=<Channel to log things to>
```

Run the bot

```shell
python -m bot
```