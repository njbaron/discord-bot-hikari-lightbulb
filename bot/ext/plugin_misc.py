import lightbulb

plugin_misc = lightbulb.Plugin("Misc", "A set of misc commands.")


@plugin_misc.command
@lightbulb.command("ping", "checks that the bot is alive")
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def ping(ctx: lightbulb.Context) -> None:
    await ctx.respond("Pong!")


@plugin_misc.command
@lightbulb.option("text", "text to repeat")
@lightbulb.command("echo", "repeats the given text")
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def echo(ctx: lightbulb.Context) -> None:
    await ctx.respond(ctx.options.text)


def load(bot: lightbulb.BotApp):
    bot.add_plugin(plugin_misc)


def unload(bot: lightbulb.BotApp):
    bot.remove_plugin(plugin_misc)
